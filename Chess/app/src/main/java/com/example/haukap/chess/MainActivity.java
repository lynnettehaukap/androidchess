package com.example.haukap.chess;

import java.util.*;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TableLayout;
import android.widget.Toast;
import android.view.MotionEvent;
import android.content.Context;
import android.content.res.Resources;
import android.widget.ScrollView;
import android.widget.LinearLayout;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.CheckBox;
import android.view.LayoutInflater;

import static com.example.haukap.chess.R.layout.activity_main;


    public class MainActivity extends AppCompatActivity {

        ChessEngine engine = new ChessEngine();
        AndroidIO io = new AndroidIO();
        AndroidRenderer renderer = new AndroidRenderer();
        Piece piece = new Piece();
        String toLoc = null;
        String fromLoc = null;
        Piece firstPiece = null;
        Piece invalidPiece = null;
        String buttonPushed;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setContentView(activity_main);
            setMsgButton();
            setRedoButton();
            setMenuButton();

            engine.initRenderer(renderer);
            engine.initIO(io);
            engine.initPiece(piece);
            engine.initChessBoard();
            updateChessBoardUI();
            loop();
            updateButtons();
        }

        private void updateButtons(){
            updateA1();
            updateB1();
            updateC1();
            updateD1();
            updateE1();
            updateF1();
            updateG1();
            updateH1();

            updateA2();
            updateB2();
            updateC2();
            updateD2();
            updateE2();
            updateF2();
            updateG2();
            updateH2();

            updateA3();
            updateB3();
            updateC3();
            updateD3();
            updateE3();
            updateF3();
            updateG3();
            updateH3();

            updateA4();
            updateB4();
            updateC4();
            updateD4();
            updateE4();
            updateF4();
            updateG4();
            updateH4();

            updateA5();
            updateB5();
            updateC5();
            updateD5();
            updateE5();
            updateF5();
            updateG5();
            updateH5();

            updateA6();
            updateB6();
            updateC6();
            updateD6();
            updateE6();
            updateF6();
            updateG6();
            updateH6();

            updateA7();
            updateB7();
            updateC7();
            updateD7();
            updateE7();
            updateF7();
            updateG7();
            updateH7();

            updateA8();
            updateB8();
            updateC8();
            updateD8();
            updateE8();
            updateF8();
            updateG8();
            updateH8();
        }
        //changes own piece and background of another piece
        private void test() {
            String loc = "b1";
            Resources res = getResources();
            int id = res.getIdentifier(loc, "id", getPackageName());
            final ImageButton bttn = (ImageButton) findViewById(id);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    bttn.setImageResource(R.drawable.green);

                    String s = "b2B";
                    Resources res = getResources();
                    int id = res.getIdentifier(s, "id", getPackageName());
                    final ImageButton btn = (ImageButton) findViewById(id);
                    btn.setImageResource(R.drawable.green);
                }
            });
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            int x = (int) event.getX();
            int y = (int) event.getY();
            String s = Integer.toString(x);
            char ch = s.charAt(0);
            loop();
            //io.setMove(x, y, engine);
            //Integer row = engine.getRow(x);
            //String col = engine.getCol(y);
            //engine.updateMove();

            return false;
        }

        private void loop() {

            engine.gameLoop();
        }

        private void setMsgButton() {
            final Button msgButton = (Button) findViewById(R.id.msgButton);

            msgButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Toast.makeText(MainActivity.this, "stuff happened",
                            Toast.LENGTH_LONG).show();
                }

            });

        }

        private void setRedoButton() {
            final Button redoButton = (Button) findViewById(R.id.redoButton);

            redoButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Toast.makeText(MainActivity.this, "redo button hit",
                            Toast.LENGTH_LONG).show();
                }
            });
        }

        private void setMenuButton() {
            final Button menuButton = (Button) findViewById(R.id.menuButton);

            menuButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    System.exit(0);
                }
            });
        }

        private void updatePiece( String loc){
            Piece p;
            Map<String, Vector<Integer> > innerMap;
            Vector<Integer> vec;
            char c = loc.charAt(0);
            char r = loc.charAt(1);
            String col = Character.toString(c);
            String currRow = Character.toString(r);
            String end = "B";
            String tempCol;//for legal move col's
            Integer row = new Integer(Character.getNumericValue(r));
            p = engine.getPieceFromChessBoard(col, row);

//engine.findAllLegalMoves();

            //buttonPushed = loc;

            // this piece is 1st piece selected
            if(firstPiece == null){

                //current player owns piece
                if (engine.isPlayerOwner(p)) {
                    firstPiece = p;
                    //change chessboard background color to display selected piece
                    String s = loc + "B";
                    Resources res = getResources();
                    int id = res.getIdentifier(s, "id", getPackageName());
                    final ImageButton bttn = (ImageButton) findViewById(id);
                    bttn.setImageResource(R.drawable.blue1);

                    engine.m_fromCol = c;
                    engine.m_fromRow = row;

                    //display legal moves
                    if (engine.m_legalMoves.containsKey(p)) {

                        innerMap = engine.m_legalMoves.get(p);

                        Iterator it = innerMap.keySet().iterator();
                        while (it.hasNext()) {
                            tempCol = (String) it.next();

                            vec = innerMap.get(tempCol);

                            for (int i = 0; i < vec.size(); ++i) {
                                Integer row1 = vec.get(i);

                                String r1 = Integer.toString(row1);
                                //String r1 = "2";
                                String s1 = tempCol + r1 + "B";
                                Resources res1 = getResources();
                                int id7 = res1.getIdentifier(s1, "id", getPackageName());
                                final ImageButton btn = (ImageButton) findViewById(id7);
                                btn.setImageResource(R.drawable.green);
                            }

                        }
                    }
                }
            }
            else{
                //2nd piece
                String color;
                engine.m_toCol = c;
                engine.m_toRow = row;

                //get square color
                color = firstPiece.m_color;
                String a = Character.toString(firstPiece.getCol());
                int r1 = firstPiece.getRow();
                String b = Integer.toString(r1);


                //unselect current piece
                String square = a + b + end;
                Resources res4 = getResources();
                int id = res4.getIdentifier(square, "id", getPackageName());
                final ImageButton current = (ImageButton) findViewById(id);
                //set color
                Resources res3 = getResources();
                int id3 = res3.getIdentifier(color, "drawable", getPackageName());
                current.setImageResource(id3 );

                //undo legal moves
                if (engine.m_legalMoves.containsKey(firstPiece)) {
                    innerMap = engine.m_legalMoves.get(firstPiece);

                    Iterator it = innerMap.keySet().iterator();
                    while (it.hasNext()) {
                        tempCol = (String) it.next();

                        vec = innerMap.get(tempCol);

                        for (int i = 0; i < vec.size(); ++i) {
                            Integer row1 = vec.get(i);

                            String r2 = Integer.toString(row1);
                            String s1 = tempCol + r2 + "B";
                            Resources res1 = getResources();
                            int id4 = res1.getIdentifier(s1, "id", getPackageName());
                            final ImageButton btn = (ImageButton) findViewById(id4);
                            //get original color for each square then set
                            Piece legalPiece = engine.getPieceFromChessBoard(tempCol, row1);
                            color = legalPiece.m_color;

                            Resources res2 = getResources();
                            int id2 = res2.getIdentifier(color, "drawable", getPackageName());
                            btn.setImageResource(id2 );

                        }

                    }

                        //process legal move
                        //verify legal move
                        if (engine.verifyLegalMove()){
                            //remove piece image from firstPiece square
                            Integer r3 = new Integer(firstPiece.getRow());
                            String s = Character.toString(firstPiece.getCol())+ r3;
                            Resources res1 = getResources();
                            int id5 = res1.getIdentifier(s, "id", getPackageName());
                            final ImageButton btn = (ImageButton) findViewById(id5);
                            btn.setImageAlpha(0);

                            //get piece type
                            char idType = firstPiece.m_pieceType;
                            String type = engine.getPieceType(idType);
                            String playerEnd = engine.getPlayerEndString();
                            type += playerEnd;//add 'w' or 'b' to image name
                            Resources res6 = getResources();
                            int id6 = res6.getIdentifier(type, "drawable", getPackageName());

                            //add image to new location
                            String s6 = col + currRow;
                            Resources res7 = getResources();
                            int id7 = res7.getIdentifier(s6, "id", getPackageName());
                            final ImageButton btn7 = (ImageButton) findViewById(id7);
                            btn7.setImageResource(id6);
                            char colF = engine.m_fromCol;
                            int rowF = engine.m_fromRow;
                            char colT = engine.m_toCol;
                            int rowT = engine.m_toRow;

                            engine.updateChessBoard();
                            engine.switchPlayer();
                            engine.m_legalMoves.clear();
                            engine.findAllLegalMoves();
                    }
                }
                //io.m_columns.clear();
               // io.m_rows.clear();
                firstPiece = null;
                //engine.m_isFirstPieceSet = false;

                //loop();
            }
        }

        private void updateChessBoardUI()
        {
            int row;
            char col;
            int y;//top, y position
            int x;

            //Col a
            col = 'a';
            ImageButton pieceButton = (ImageButton) findViewById(R.id.a1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.a2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.a3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.a4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.a5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.a6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.a7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.a8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);

            //col b
            col = 'b';
            pieceButton = (ImageButton) findViewById(R.id.b1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.b2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.b3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.b4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.b5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.b6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.b7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.b8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);


            //col c
            col = 'c';
            pieceButton = (ImageButton) findViewById(R.id.c1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.c2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.c3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.c4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.c5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.c6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.c7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.c8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);


            //col d
            col = 'd';
            pieceButton = (ImageButton) findViewById(R.id.d1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.d2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.d3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.d4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.d5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.d6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.d7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.d8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);

            //col e
            col = 'e';
            pieceButton = (ImageButton) findViewById(R.id.e1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.e2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.e3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.e4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.e5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.e6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.e7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.e8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);

            //col f
            col = 'f';
            pieceButton = (ImageButton) findViewById(R.id.f1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.f2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.f3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.f4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.f5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.f6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.f7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.f8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);

            //col g
            col = 'g';
            pieceButton = (ImageButton) findViewById(R.id.g1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.g2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.g3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.g4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.g5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.g6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.g7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.g8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);

            //col h
            col = 'h';
            pieceButton = (ImageButton) findViewById(R.id.h1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.h2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.h3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.h4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.h5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.h6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.h7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.h8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);
        }


        private String generateResourceString(String col, Integer row) {
            String r = Integer.toString(row);
            String end = "B";
            String res = col;
            res += r;
            res += end;
            return res;
        }

        private void displayLegalMove(String col, Vector<Integer> vec){
            String loc;
            String board = "B";
            Integer row;
            //iter over rows and display image
            for(int i = 0; i < vec.size(); ++i) {

                row = vec.get(i);
                loc = col + Integer.toString(row) + board;

                //display image for legal move
                Resources res = getResources();
                int id = res.getIdentifier(loc, "id", getPackageName());
                final ImageButton bttn = (ImageButton) findViewById(id);
                bttn.setImageResource(R.drawable.green);

            }
        }


        //ROW 1
        private void updateA1() {
            final String loc = "a1";
            final ImageButton bttn = (ImageButton) findViewById(R.id.a1);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateB1(){
            final String loc = "b1";
            final ImageButton bttn = (ImageButton) findViewById(R.id.b1);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    //final ImageButton btn = (ImageButton) findViewById(R.id.b1B);
                    //btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);
                }
            });
        }

        private void updateC1(){
            final String loc = "c1";
            final ImageButton bttn = (ImageButton) findViewById(R.id.c1);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    //final ImageButton btn = (ImageButton) findViewById(R.id.c1B);
                    //btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);
                }
            });
        }

        private void updateD1(){
            final String loc = "d1";
            final ImageButton bttn = (ImageButton) findViewById(R.id.d1);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    //final ImageButton btn = (ImageButton) findViewById(R.id.d1B);
                    //btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);
                }
            });
        }

        private void updateE1(){
            final String loc = "e1";
            final ImageButton bttn = (ImageButton) findViewById(R.id.e1);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    //final ImageButton btn = (ImageButton) findViewById(R.id.e1B);
                    //btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);
                }
            });
        }

        private void updateF1(){
            final String loc = "f1";
            final ImageButton bttn = (ImageButton) findViewById(R.id.f1);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    //final ImageButton btn = (ImageButton) findViewById(R.id.f1B);
                    //btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);
                }
            });
        }

        private void updateG1(){
            final String loc = "g1";
            final ImageButton bttn = (ImageButton) findViewById(R.id.g1);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    //final ImageButton btn = (ImageButton) findViewById(R.id.g1B);
                    ///btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);
                }
            });
        }


        private void updateH1(){
            final String loc = "h1";
            final ImageButton bttn = (ImageButton) findViewById(R.id.h1);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    //final ImageButton btn = (ImageButton) findViewById(R.id.h1B);
                    //btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);
                }
            });
        }

        //ROW 2
        private void updateA2() {
            final String loc = "a2";
            final ImageButton bttn = (ImageButton) findViewById(R.id.a2);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    //final ImageButton btn = (ImageButton) findViewById(R.id.a2B);
                    //btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);

                }
            });
        }



        private void updateB2(){
            final String loc = "b2";
            final ImageButton bttn = (ImageButton) findViewById(R.id.b2);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    //final ImageButton btn = (ImageButton) findViewById(R.id.b2B);
                    //btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);
                }
            });
        }

        private void updateC2(){
            final String loc = "c2";
            final ImageButton bttn = (ImageButton) findViewById(R.id.c2);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    //final ImageButton btn = (ImageButton) findViewById(R.id.c2B);
                    //btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);
                }
            });
        }

        private void updateD2(){
            final String loc = "d2";
            final ImageButton bttn = (ImageButton) findViewById(R.id.d2);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    //final ImageButton btn = (ImageButton) findViewById(R.id.d2B);
                    //btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);
                }
            });
        }

        private void updateE2(){
            final String loc = "e2";
            final ImageButton bttn = (ImageButton) findViewById(R.id.e2);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    //final ImageButton btn = (ImageButton) findViewById(R.id.e2B);
                    //btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);
                }
            });
        }

        private void updateF2(){
            final String loc = "f2";
            final ImageButton bttn = (ImageButton) findViewById(R.id.f2);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    //final ImageButton btn = (ImageButton) findViewById(R.id.f2B);
                    //btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);
                }
            });
        }

        private void updateG2(){
            final String loc = "g2";
            final ImageButton bttn = (ImageButton) findViewById(R.id.g2);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    //final ImageButton btn = (ImageButton) findViewById(R.id.g2B);
                    //btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);
                }
            });
        }


        private void updateH2(){
            final String loc = "h2";
            final ImageButton bttn = (ImageButton) findViewById(R.id.h2);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    //final ImageButton btn = (ImageButton) findViewById(R.id.h2B);
                    //btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);
                }
            });
        }

        //ROW 3
        private void updateA3() {
            final String loc = "a3";
            final ImageButton bttn = (ImageButton) findViewById(R.id.a3);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    //final ImageButton btn = (ImageButton) findViewById(R.id.a3B);
                    //btn.setImageResource(R.drawable.blue1);
                    updatePiece(loc);
                }
            });
        }

        private void updateB3(){
            final String loc = "b3";
            final ImageButton bttn = (ImageButton) findViewById(R.id.b3);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateC3(){
            final String loc = "c3";
            final ImageButton bttn = (ImageButton) findViewById(R.id.c3);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateD3(){
            final String loc = "d3";
            final ImageButton bttn = (ImageButton) findViewById(R.id.d3);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateE3(){
            final String loc = "e3";
            final ImageButton bttn = (ImageButton) findViewById(R.id.e3);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateF3(){
            final String loc = "f3";
            final ImageButton bttn = (ImageButton) findViewById(R.id.f3);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateG3(){
            final String loc = "g3";
            final ImageButton bttn = (ImageButton) findViewById(R.id.g3);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateH3(){
            final String loc = "h3";
            final ImageButton bttn = (ImageButton) findViewById(R.id.h3);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        //ROW 4
        private void updateA4(){
            final String loc = "a4";
            final ImageButton bttn = (ImageButton) findViewById(R.id.a4);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateB4(){
            final String loc = "b4";
            final ImageButton bttn = (ImageButton) findViewById(R.id.b4);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateC4(){
            final String loc = "c4";
            final ImageButton bttn = (ImageButton) findViewById(R.id.c4);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateD4(){
            final String loc = "d4";
            final ImageButton bttn = (ImageButton) findViewById(R.id.d4);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateE4(){
            final String loc = "e4";
            final ImageButton bttn = (ImageButton) findViewById(R.id.e4);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateF4(){
            final String loc = "f4";
            final ImageButton bttn = (ImageButton) findViewById(R.id.f4);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateG4(){
            final String loc = "g4";
            final ImageButton bttn = (ImageButton) findViewById(R.id.g4);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateH4(){
            final String loc = "h4";
            final ImageButton bttn = (ImageButton) findViewById(R.id.h4);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }


        //ROW 5
        private void updateA5(){
            final String loc = "a5";
            final ImageButton bttn = (ImageButton) findViewById(R.id.a5);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateB5(){
            final String loc = "b5";
            final ImageButton bttn = (ImageButton) findViewById(R.id.b5);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateC5(){
            final String loc = "c5";
            final ImageButton bttn = (ImageButton) findViewById(R.id.c5);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateD5(){
            final String loc = "d5";
            final ImageButton bttn = (ImageButton) findViewById(R.id.d5);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateE5(){
            final String loc = "e5";
            final ImageButton bttn = (ImageButton) findViewById(R.id.e5);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateF5(){
            final String loc = "f5";
            final ImageButton bttn = (ImageButton) findViewById(R.id.f5);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateG5(){
            final String loc = "g5";
            final ImageButton bttn = (ImageButton) findViewById(R.id.g5);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateH5(){
            final String loc = "h5";
            final ImageButton bttn = (ImageButton) findViewById(R.id.h5);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }


        //ROW 6
        private void updateA6(){
            final String loc = "a6";
            final ImageButton bttn = (ImageButton) findViewById(R.id.a6);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateB6(){
            final String loc = "b6";
            final ImageButton bttn = (ImageButton) findViewById(R.id.b6);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateC6(){
            final String loc = "c6";
            final ImageButton bttn = (ImageButton) findViewById(R.id.c6);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateD6(){
            final String loc = "d6";
            final ImageButton bttn = (ImageButton) findViewById(R.id.d6);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateE6(){
            final String loc = "e6";
            final ImageButton bttn = (ImageButton) findViewById(R.id.e6);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateF6(){
            final String loc = "f6";
            final ImageButton bttn = (ImageButton) findViewById(R.id.f6);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateG6(){
            final String loc = "g6";
            final ImageButton bttn = (ImageButton) findViewById(R.id.g6);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateH6(){
            final String loc = "h6";
            final ImageButton bttn = (ImageButton) findViewById(R.id.h6);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }


        //ROW 7
        private void updateA7(){
            final String loc = "a7";
            final ImageButton bttn = (ImageButton) findViewById(R.id.a7);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateB7(){
            final String loc = "b7";
            final ImageButton bttn = (ImageButton) findViewById(R.id.b7);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateC7(){
            final String loc = "c7";
            final ImageButton bttn = (ImageButton) findViewById(R.id.c7);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateD7(){
            final String loc = "d7";
            final ImageButton bttn = (ImageButton) findViewById(R.id.d7);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateE7(){
            final String loc = "e7";
            final ImageButton bttn = (ImageButton) findViewById(R.id.e7);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateF7(){
            final String loc = "f7";
            final ImageButton bttn = (ImageButton) findViewById(R.id.f7);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateG7(){
            final String loc = "g7";
            final ImageButton bttn = (ImageButton) findViewById(R.id.g7);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateH7(){
            final String loc = "h7";
            final ImageButton bttn = (ImageButton) findViewById(R.id.h7);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }


        //ROW 8
        private void updateA8(){
            final String loc = "a8";
            final ImageButton bttn = (ImageButton) findViewById(R.id.a8);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateB8(){
            final String loc = "b8";
            final ImageButton bttn = (ImageButton) findViewById(R.id.b8);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateC8(){
            final String loc = "c8";
            final ImageButton bttn = (ImageButton) findViewById(R.id.c8);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateD8(){
            final String loc = "d8";
            final ImageButton bttn = (ImageButton) findViewById(R.id.d8);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateE8(){
            final String loc = "e8";
            final ImageButton bttn = (ImageButton) findViewById(R.id.e8);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateF8(){
            final String loc = "f8";
            final ImageButton bttn = (ImageButton) findViewById(R.id.f8);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateG8(){
            final String loc = "g8";
            final ImageButton bttn = (ImageButton) findViewById(R.id.g8);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateH8(){
            final String loc = "h8";
            final ImageButton bttn = (ImageButton) findViewById(R.id.h8);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    updatePiece(loc);
                }
            });
        }

        private void updateChessBoard() {
            //final int resID = getResources().getIdentifier(s , "drawable", getPackageName());

            Resources res = getResources();
            int id = res.getIdentifier(fromLoc, "id", getPackageName());
            final ImageButton bttn = (ImageButton) findViewById(id);

            bttn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    //String s = "rook_w";

                    //Context c = getApplicationContext();
                    //int id2 = c.getResources().getIdentifier(s, "id", getPackageName());
                    // int id1 = c.getResources().getIdentifier("drawable/"+ s, null, c.getPackageName());

                    //Resources res2 = getResources();
                    //int id2 = c.getResources().getIdentifier(img, "id", getPackageName());
                    //Drawable image = getResources().getDrawable(id2);

                    toLoc = "a3";
                    if(toLoc != null)
                        System.exit(0);
                    Resources res = getResources();
                    int id = res.getIdentifier(fromLoc, "id", getPackageName());
                    final ImageButton btn = (ImageButton) findViewById(id);

                    Resources res1 = getResources();
                    int id1 = res1.getIdentifier(toLoc, "id", getPackageName());
                    //final ImageButton bttn = (ImageButton) findViewById(id1);
                    btn.setImageResource(id1);

                }
            });
            //return true;
        }

    public void changeButtonText(ImageView imageview)
    {
       // imageview = (ImageView)findViewById(R.id.imageViewA);
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.diagram);

        imageview.setImageDrawable(drawable);
    }

/*
        private void updateChessBoardUI()
        {
            int row;
            char col;
            int y;//top, y position
            int x;

            //Col a
            col = 'a';
            ImageButton pieceButton = (ImageButton) findViewById(R.id.a1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.a2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.a3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.a4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.a5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.a6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.a7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.a8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);

            //col b
            col = 'b';
            pieceButton = (ImageButton) findViewById(R.id.b1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.b2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.b3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.b4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.b5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.b6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.b7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.b8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);


            //col c
            col = 'c';
            pieceButton = (ImageButton) findViewById(R.id.c1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.c2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.c3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.c4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.c5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.c6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.c7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.c8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);


            //col d
            col = 'd';
            pieceButton = (ImageButton) findViewById(R.id.d1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.d2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.d3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.d4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.d5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.d6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.d7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.d8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);

            //col e
            col = 'e';
            pieceButton = (ImageButton) findViewById(R.id.e1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.e2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.e3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.e4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.e5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.e6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.e7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.e8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);

            //col f
            col = 'f';
            pieceButton = (ImageButton) findViewById(R.id.f1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.f2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.f3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.f4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.f5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.f6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.f7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.f8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);

            //col g
            col = 'g';
            pieceButton = (ImageButton) findViewById(R.id.g1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.g2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.g3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.g4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.g5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.g6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.g7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.g8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);

            //col h
            col = 'h';
            pieceButton = (ImageButton) findViewById(R.id.h1);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 1;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.h2);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 2;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.h3);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 3;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.h4);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 4;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.h5);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 5;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.h6);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 6;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.h7);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 7;
            engine.addPieceLocation(x, y, col, row);

            pieceButton = (ImageButton) findViewById(R.id.h8);
            y = pieceButton.getTop();//y position
            x = pieceButton.getLeft();
            row = 8;
            engine.addPieceLocation(x, y, col, row);
        }
        */

    }
