package com.example.haukap.chess;
/**
 *  Created by haukap on 7/17/17.
 */

public interface ChessIO
{
    void setMove(char col, int row);
}