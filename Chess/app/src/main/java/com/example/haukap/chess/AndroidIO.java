package com.example.haukap.chess;

/**
 *   Created by haukap on 7/25/17.
 */
import java.util.*;

public class AndroidIO implements ChessIO
{
    char m_colT = 'z';
    char m_colF = 'z';

    int m_rowT = 0;
    int m_rowF = 0;
    int m_size = 2;
    boolean moveSelected = false;
    public Vector<String> m_columns;
    public Vector<Integer> m_rows;
    //boolean fromMoveSet = false;

    AndroidIO(){}

    public void setMove(char col, int row)
    {
        Integer r = new Integer(row);
        String c = Character.toString(col);
        m_columns.addElement(c);
        m_rows.addElement(r);
        m_colF = col;
        m_rowF = row;

        //"FROM" piece selected
        if(m_columns.size() != m_size)
        {
            moveSelected = false;
        }
        else
        {
            //"TO" piece selected
            moveSelected = true;
        }
    }
}